package com.sellstop.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;

import com.sellstop.model.Customer;



public class CustomerDaoImpl implements CustomerDao{

	@Autowired
	SessionFactory sessionFactory;
	List<Customer> customerList = new ArrayList<Customer>();
	Customer customer = new Customer();
	HibernateTemplate template;  
	public void setTemplate(HibernateTemplate template) {  
	    this.template = template;  
	}  
	  
	@Override
	public void register(Customer customer){  
		
		Session session = sessionFactory.openSession();
		session.save(customer);  
		session.close();
	}

	/*@SuppressWarnings("unchecked")
	@Override
	public List<Customer> getCustomerByUsername(String username) {
		Session session = sessionFactory.openSession();
		
		Query customerCount = session.createQuery("select COUNT(*) from Customer");
		List<String> countList = new ArrayList<String>(); 
		countList = customerCount.list();
		String count = String.valueOf(countList.get(0));
		System.out.println("count : "+count);
		
		if(!count.equals("0")){
			Query getCustomer = session.createQuery("from Customer where username ='"+username+"'");
			customerList = getCustomer.list();
			System.out.println("query : "+customerList.get(0).getUsername());
		}else{
			return null;
		}

		session.close();
		return customerList;
	}*/

/*	@Override
	public List<Customer> login(Customer customer) {
		Session session = sessionFactory.openSession();
		Query getCustomer = session.createQuery("from Customer where username ='"+customer.getUsername()+"'");
		customerList = getCustomer.list();
		session.close();
		return customerList;
		
	}*/

	/*public Customer getCustomerByUsernameAndPassword(String username,
			String password) {
		Session session = sessionFactory.openSession();
		Criteria criteria = null;
		//Query getCustomer = session.createQuery("from Customer where username ='"+username+"' and password ="+password);
		//customerList = getCustomer.;
		try{
			criteria = session.createCriteria(Customer.class);
			Criterion usernameCriterion = Restrictions.eq("username", username);
			Criterion passwordCriterion = Restrictions.eq("password", password);
			criteria.add(usernameCriterion).add(passwordCriterion);
			criteria.setMaxResults(1);
			customer = (Customer) criteria.uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return customer;
	}  */


}
