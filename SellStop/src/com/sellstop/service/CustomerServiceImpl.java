package com.sellstop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sellstop.dao.CustomerDao;
import com.sellstop.model.Customer;


@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerDao customerDao;
	@Override
	public void register(Customer customer) {
		//boolean customerAlreadyExists = checkExistingCustomer(customer);
	//	if(customerAlreadyExists){
		//	return;
		//}else{
			/**RAJ 20 Jan 2018
			 * Encode the password*/
			
			
			customerDao.register(customer);
		}

	}
/*	private boolean checkExistingCustomer(Customer customer) {
		String email = customer.getEmail();
		String username = customer.getUsername();
		
		List<Customer> existingCustomer = customerDao.getCustomerByUsername(username);
		
		if(existingCustomer !=null && !existingCustomer.isEmpty() && email.equalsIgnoreCase(existingCustomer.get(0).getEmail())
				&& username.equalsIgnoreCase(existingCustomer.get(0).getUsername())){
			return true;	
		}
		
		return false;
	}*/
/*	@Override
	public Customer login(Customer customer) {
		String username = customer.getUsername();
		String password = customer.getPassword();
		Customer loggedCustomer = customerDao.getCustomerByUsernameAndPassword(username, password);
		return loggedCustomer;
	}
*/


