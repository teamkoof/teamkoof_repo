package com.sellstop.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sellstop.model.Customer;
import com.sellstop.service.CustomerService;


@Controller
public class CutomerController {

	@Autowired
	CustomerService customerService;
	
	@RequestMapping(value="/", method=RequestMethod.GET)
		public String home(ModelMap model, @ModelAttribute("customer")Customer customer){
			return "home";
		}
	
	@RequestMapping(value="/test", method=RequestMethod.GET)
	public String test(){
		return "home";
	}
	
	@RequestMapping(value="/register", method=RequestMethod.POST)
	public String register(@ModelAttribute("customer")Customer customer){
		customerService.register(customer);
		return "home";
	}
	
	
	@RequestMapping(value="/Access_Denied", method=RequestMethod.POST)
	public String accessDenied(){
		return "Access_Denied";
	}
	
/*	@RequestMapping(value="/login", method=RequestMethod.POST)
	public ModelAndView login(@ModelAttribute("customer")Customer customer){
		Customer loggedCustomer = customerService.login(customer);
		ModelAndView model = new ModelAndView("store");
		model.addObject("loggedCustomer", loggedCustomer);
		return model;
	}*/
}
