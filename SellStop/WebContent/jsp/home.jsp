<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
  <!-- Mannual CSS -->
  
  <%-- <spring:url value="/css/style.css" var="globalCss"></spring:url> --%>
    <link rel="stylesheet" href="resources/css/style.css" type="text/css"/>
<title>SellStop | Home</title>
</head>
<body>
<div class="jumbotron">
	<div class="row">
		<div class="col-sm-5 col-sm-offset-2">
				<h2><span class="front">Sell</span><span class="back">Stop</span></h2>
				<small>Sell your stuff online</small>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-4 col-sm-offset-2">
			<div class="registrationContainer">
			<h2>Registration</h2>
			<hr>
				<form:form action="register" method="POST" modelAttribute="customer">
				<div class="form-group">
					<!-- <input type="text" name="name" id="name" placeholder="Name" class="form-control fieldDesign"/> -->
					<form:input path="name" id="name" placeholder="Name" class="form-control fieldDesign"></form:input>
				</div>
				<div class="form-group">
					<!-- <input type="text" name="email" id="email" placeholder="Email" class="form-control fieldDesign"/> -->
					<form:input path="email" id="email" placeholder="Email" class="form-control fieldDesign"></form:input>
				</div>
				<div class="form-group">
					<!-- <input type="text" name="city" id="city" placeholder="City" class="form-control fieldDesign"/> -->
					<form:input path="city" id="city" placeholder="City" class="form-control fieldDesign"></form:input>
				</div>
				<div class="form-group">
					<!-- <input type="text" name="state" id="state" placeholder="State" class="form-control fieldDesign"/> -->
					<form:input path="state" id="state" placeholder="State" class="form-control fieldDesign"></form:input>
				</div>
				<div class="form-group">
					<!-- <input type="text" name="mobile" id="mobile" placeholder="Mobile" class="form-control fieldDesign"/> -->
					<form:input path="mobile" id="mobile" placeholder="Mobile" class="form-control fieldDesign"></form:input>
				</div>
				<div class="form-group">
					<!-- <input type="text" name="state" id="state" placeholder="State" class="form-control fieldDesign"/> -->
					<form:input path="user.username" id="username" placeholder="Username" class="form-control fieldDesign"></form:input>
				</div>
				<div class="form-group">
					<!-- <input type="text" name="mobile" id="mobile" placeholder="Mobile" class="form-control fieldDesign"/> -->
					<form:input path="user.password" id="password" placeholder="Password" class="form-control fieldDesign"></form:input>
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Register" style="width:300px;"/>
				</div>
				</form:form>
				<a href="test">click</a>
			</div>
		</div>
		<div class="col-sm-4 col-sm-offset-2">
			<div class="loginContainer">
			<h2>Login Here</h2>
			<hr>
				<form action="login" method="POST">
				<div class="form-group">
					<input type="text" name="username" id="username" placeholder="Username" class="form-control fieldDesign"/>
				</div>
				<div class="form-group">
					<input type="password" name="password" id="password" placeholder="Password" class="form-control fieldDesign"/>
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-success" value="Login" style="width:300px;"/>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
</body>
</html>