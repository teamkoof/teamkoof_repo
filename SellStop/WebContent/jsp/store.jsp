<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
  <!-- Mannual CSS -->
  
  <%-- <spring:url value="/css/style.css" var="globalCss"></spring:url> --%>
    <link rel="stylesheet" href="resources/css/style.css" type="text/css"/>
<title>SellStop | Store</title>
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#myPage"><span class="front">Sell</span><span class="back">Stop</span></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right" style="padding-top:10px;">
      	<li><a href="#">Welcome, ${loggedCustomer.name}</a></li>
      	<li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Cart</a></li>
        <li><button class="sellStopbtn">Sign Out</button></li>
      </ul>
    </div>
  </div>
</nav>
<div class="emptyArea1">
</div>
<div class="storeArea">
<div class="container-fluid text-center">
	<h2 style="color:#F13751;">Start Shopping</h2>
	<br><br>
<div class="row">
	<div class="col-sm-4">
		<div class="panel panel-default">
		    <div class="panel-body">
		    <img src="resources/images/store/c3.jpg" width="300px" height="300px;">
		    </div>
		    <div class="panel-footer">
		    <b>Price :</b> &#8377 599<br>
		    <button class="addToCartbtn">Add To Cart</button>
		    </div>
		  </div>
	</div>
	<div class="col-sm-4">
		<div class="panel panel-default">
		    <div class="panel-body">
		    <img src="resources/images/store/c2.jpg" width="300px" height="300px;">
		    </div>
		    <div class="panel-footer">
		    <b>Price :</b> &#8377 899<br>
		    <button class="addToCartbtn">Add To Cart</button>
		    </div>
		  </div>
	</div>
	<div class="col-sm-4">
		<div class="panel panel-default">
		    <div class="panel-body">
		    <img src="resources/images/store/c1.jpg" width="300px" height="300px;">
		    </div>
		    <div class="panel-footer">
		    <b>Price :</b> &#8377 699<br>
		    <button class="addToCartbtn">Add To Cart</button>
		    </div>
		  </div>
	</div>
</div>
<div class="row">
	<div class="col-sm-4">
		<div class="panel panel-default">
		    <div class="panel-body">
		    <img src="resources/images/store/c4.jpg" width="300px" height="300px;">
		    </div>
		    <div class="panel-footer">
		    <b>Price :</b> &#8377 700<br>
		    <button class="addToCartbtn">Add To Cart</button>
		    </div>
		  </div>
	</div>
	<div class="col-sm-4">
		<div class="panel panel-default">
		    <div class="panel-body">
		    <img src="resources/images/store/c5.jpg" width="300px" height="300px;">
		    </div>
		    <div class="panel-footer">
		    <b>Price :</b> &#8377 1500<br>
		    <button class="addToCartbtn">Add To Cart</button>
		    </div>
		  </div>
	</div>
	<div class="col-sm-4">
		<div class="panel panel-default">
		    <div class="panel-body">
		    <img src="resources/images/store/c6.jpg" width="300px" height="300px;">
		    </div>
		    <div class="panel-footer">
		    <b>Price :</b> &#8377 1020<br>
		    <button class="addToCartbtn">Add To Cart</button>
		    </div>
		  </div>
	</div>
</div>
<div class="row">
	<div class="col-sm-4">
		<div class="panel panel-default">
		    <div class="panel-body">
		    <img src="resources/images/store/c7.jpg" width="300px" height="300px;">
		    </div>
		    <div class="panel-footer">
		    <b>Price :</b> &#8377 830<br>
		    <button class="addToCartbtn">Add To Cart</button>
		    </div>
		  </div>
	</div>
	<div class="col-sm-4">
		<div class="panel panel-default">
		    <div class="panel-body">
		    <img src="resources/images/store/c8.jpg" width="300px" height="300px;">
		    </div>
		    <div class="panel-footer">
		    <b>Price :</b> &#8377 530<br>
		    <button class="addToCartbtn">Add To Cart</button>
		    </div>
		  </div>
	</div>
	<div class="col-sm-4">
		<div class="panel panel-default">
		    <div class="panel-body">
		    <img src="resources/images/store/c9.jpg" width="300px" height="300px;">
		    </div>
		    <div class="panel-footer">
		    <b>Price :</b> &#8377 400<br>
		    <button class="addToCartbtn">Add To Cart</button>
		    </div>
		  </div>
	</div>
</div>
</div>
</div>
</body>
</html>